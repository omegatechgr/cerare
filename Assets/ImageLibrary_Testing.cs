﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Omega.Settings;
using Omega.Framework;
using Cerare.Framework.Managers;
using Cerare.Framework.Data;
using Cerare.Framework.Libraries;

public class ImageLibrary_Testing : MonoBehaviour
{

    [SerializeField] private string m_sessionId;
    [SerializeField] private Product m_product;

    private void Awake()
    {
#if !CERARE_TESTING
        Destroy(this.gameObject);
#endif
    }

    private void Start()
    {
        RestManager.GetAccess(AppEngine.Settings.SecretAppID,
            accessResponse =>
            {
                m_sessionId = accessResponse.SessionId;

                RestManager.GetProduct(AppEngine.Settings.CeramistID, AppEngine.Settings.ProductID,
                    product =>
                    {
                        m_product = product;
                        Debug.LogFormat("Got Product {0} from {1} with id {2} ", product.Name, product.Creator, product.Id);

                        List<Asset> ImageAssets = new List<Asset>();

                        for (int i = 0; i < m_product.StateAssetCount; i++)
                        {
                            if (m_product.Assets[i].GetType() == typeof(Cerare.Framework.Data.Image))
                            {
                                Debug.LogFormat("Asset is type of {0}", m_product.Assets[i].GetType());

                                ImageAssets.Add(m_product.Assets[i]);
                            }
                        }

                        ImageLibrary.s_ImageLibrary.GetAssets(ImageAssets, () => { Debug.LogFormat("Added {0} image assets on image library", ImageAssets.Count); });
                    },
                    error =>
                    {
                        Debug.LogFormat("Rest Error \n Code: {0} \n Error Description: {1}", error.Code, error.Description);
                    });
            }
            , error =>
            {
                Debug.LogFormat("Rest Error \n Code: {0} \n Error Description: {1}", error.Code, error.Description);
            });
    }
}
