﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Omega.Rest;
using Omega.Utils;
using Omega.Framework;
using System;
using RestError = Omega.Rest.RestUtil.RestCallError;
using Cerare.Framework.Api;
using Cerare.Framework.Data;
using UnityEngine.Networking;

namespace Cerare.Framework.Managers
{
    public class RestManager : MonoBehaviour {
        
        private const string GET_ACCESS = "CerareAPI/login?secretAppID={0}";
        private const string GET_PRODUCT = "CerareAPI/Getproduct?sessionID={0}&ceramistID={1}&productID={2}";
        private const string GET_ASSET = "CerareAPI/Getasset?sessionID={0}&assetID={1}";

        public static RestManager instance;
        private RestUtil restUtil;
        private static string m_sessionID;

        public void Awake()
        {
            if (instance != null)
            {
                Destroy(gameObject);
                return;
            }

            instance = this;
            DontDestroyOnLoad(gameObject);

            restUtil = RestUtil.Initialize(this);
        }

        public static void GetAccess(string secretAppID,
            Action<AccessResponse> onCompletion, Action<RestError> onError)
        {
            WebRequestBuilder builder = new WebRequestBuilder()
                .Url(GetApiUrl(GetHostUrl(GET_ACCESS), secretAppID))
                .Verb(Verbs.GET);

            instance.restUtil.Send(builder,
                handler =>
                {
                    Debug.Log(handler.text);
                    AccessResponse response =  new AccessResponse();
                    response = JsonManager.DeserializedJson<AccessResponse>(response,handler.text);

                    m_sessionID = response.SessionId;

                    if (onCompletion != null)
                        onCompletion(response);
                },
                restError =>
                {
                    if (onError != null)
                        onError(restError);
                });
        }

        public static void GetProduct(string ceramistID,string productID,
            Action<Product> onCompletion, Action<RestError> onError)
        {
            WebRequestBuilder builder = new WebRequestBuilder()
                .Url(GetApiUrl(GetHostUrl(GET_PRODUCT), m_sessionID,ceramistID,productID))
                .Verb(Verbs.GET);

            instance.restUtil.Send(builder,
                handler =>
                {
                    Debug.Log(handler.text);
                    Product response =  new Product();
                    response = JsonManager.DeserializedJson<Product>(response,handler.text);

                    if (onCompletion != null)
                        onCompletion(response);
                },
                restError =>
                {
                    if (onError != null)
                        onError(restError);
                });
        }

        public static void GetImage(int assetID,
            Action<Texture> onCompletion, Action<RestError> onError)
        {
            WebRequestBuilder builder = new WebRequestBuilder()
                .Url(GetApiUrl(GetHostUrl(GET_ASSET), m_sessionID, assetID.ToString()))
                .Verb(Verbs.GET)
                .Handler(new DownloadHandlerTexture());

            instance.restUtil.Send(builder,
                handler =>
                {
                    if (onCompletion != null)
                        onCompletion(((DownloadHandlerTexture)handler).texture);
                },
                restError =>
                {
                    if(onError!=null)
                        onError(restError);
                });
        }

        public static void GetAudio(int assetID,
           Action<AudioClip> onCompletion, Action<RestError> onError)
        {
            WebRequestBuilder builder = new WebRequestBuilder()
                .Url(GetApiUrl(GetHostUrl(GET_ASSET), m_sessionID, assetID.ToString()))
                .Verb(Verbs.GET)
                .Handler(new DownloadHandlerAudioClip(GetApiUrl(GetHostUrl(GET_ASSET), m_sessionID, assetID.ToString()),AudioType.MPEG));

            instance.restUtil.Send(builder,handler =>
             {
                 if (onCompletion != null)
                     onCompletion(((DownloadHandlerAudioClip)handler).audioClip);
             }, restError =>
             {
                 if (onError != null)
                     onError(restError);
             });
        }

        public static string GetHostUrl(string format)
        {
            return string.Format("{0}/{1}/{2}", AppEngine.Settings.RestHost, AppEngine.Settings.ApiKey,format);
        }

        public static string GetApiUrl(string format,string arg1,string arg2=null,string arg3=null)
        {
            if (string.IsNullOrEmpty(arg3))
            {
                if(string.IsNullOrEmpty(arg2))
                    return string.Format(format,arg1);
                else
                    return string.Format(format, arg1,arg2);
            }
            else
                return string.Format(format, arg1,arg2,arg3);
        }
    }
}
