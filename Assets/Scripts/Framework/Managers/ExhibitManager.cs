﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.MessageKit;
using Omega.Utils;
using Omega.Framework;
using Cerare.Framework.Data;
namespace Cerare.Framework.Managers
{
    public class ExhibitManager : MonoBehaviour {

        public static ExhibitManager instance;

        #pragma warning disable 649
        [Header("Testing")]
        [SerializeField] private TextAsset json01;
        [SerializeField] private TextAsset json02;
        #pragma warning restore

        private Product loadedState;

        private void Awake()
        {
            if (instance != null)
                Destroy (instance);
            instance = this;

            MessageKit.addObserver (MessageType.ScannedNewMarker, getState);
        }

        private void OnDestroy()
        {
            MessageKit.removeObserver (MessageType.ScannedNewMarker, getState);
        }

        private void getState()
        {
            // TODO :: remove this part and replace it with WebRequest to get state
            if (ScannedData.MarkerID == "01")
                loadedState = JsonManager.DeserializedJson<Product> (loadedState, json01.text);
            else
                loadedState = JsonManager.DeserializedJson<Product> (loadedState, json02.text);
        }

        private void createStateBehaviour()
        {
            for (int i = 0; i < loadedState.StateAssetCount; i++) 
            {
                if (loadedState.Assets [i].DataType == DataType.Image) 
                {

                } 
                else if (loadedState.Assets [i].DataType == DataType.Video) 
                {

                } 
                else if (loadedState.Assets [i].DataType == DataType.Text) 
                {

                }
            }
        }
    }
}
