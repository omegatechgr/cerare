﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using System;

namespace Cerare.Framework.Managers
{
    public class ARManager : MonoBehaviour {

        [SerializeField] public Action interaction;
        public static ARManager instance; 

        [SerializeField] private bool AR_enabled=true;

        private void Awake()
        {
            if (instance != null)
                Destroy (instance);
            instance = this;

            VuforiaRuntime.Instance.InitVuforia();

        }

        private void interact()
        {
            interaction();
        }
    }
}
