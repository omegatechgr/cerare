﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Prime31.MessageKit;
using Omega.Framework;
public class ARUIManager : MonoBehaviour {

    #pragma warning disable 649
    [SerializeField] private Image markerPlaceholder;
    #pragma warning restore

    private void Awake()
    {
        MessageKit.addObserver (MessageType.ScannedNewMarker, disableMarkerPlaceholder);
    }

    private void OnDestroy()
    {
        MessageKit.removeObserver (MessageType.ScannedNewMarker, disableMarkerPlaceholder);
    }

    private void disableMarkerPlaceholder()
    {
        markerPlaceholder.gameObject.SetActive (false);
    }
}
