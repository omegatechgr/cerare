﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.MessageKit;
using Cerare.Framework.Data;
using Cerare.Framework.Managers;
using Omega.Framework;
using Omega.Utils;

namespace Cerare.Framework.Libraries
{
    public class ImageLibrary : MonoBehaviour
    {
        public static  ImageLibrary s_ImageLibrary;

        GenericAssetLibrary<ImageLibraryAsset, Texture> m_ImageLibrary;
        Dictionary<int, Asset> m_AssetDictionary;

        int m_InitializationIndex;

        private void Awake()
        {
            InitSingleton();

            m_InitializationIndex = 0;

            //show loading screen
            /*Send message here to enable loading screen*/

            //create images library
            m_ImageLibrary = new GenericAssetLibrary<ImageLibraryAsset, Texture>(
                (id, method, errorDelegate) => RestManager.GetImage(id, (texture) => method(id, texture), (x) => errorDelegate()),
                ImageLibraryAsset.CreateAsset
            );
        }

        public void GetAssets(List<Asset> assets,Action callback)
        {
            OnGetAssets(assets, callback);
        }

        void OnGetAssets(List<Asset> assets, Action callback)
        {
            if (assets == null)
            {
                Debug.Log("Object asset is null");

                if (callback != null)
                    callback();

                return;
            }

            Debug.LogFormat("{0} object assets in product.", assets.Count);

            //create Id->Asset dictionary
            m_AssetDictionary = assets.ToIdAssetDictionary();

            //get images from server
            /*for (int i = 0; i < assets.ToIdsList().Count; i++)
            {
                Debug.Log(assets.ToIdsList()[i]);
            }
            OnGetImageAssets(callback);*/
            m_ImageLibrary.GetMultipleAssets(assets.ToIdsList(), (x) => OnGetImageAssets(callback));
        }

        void OnGetImageAssets(Action callback)
        {
            Debug.Log("Image Assets downloaded.");

            //*send message that images were downloaded*/

            if (callback != null) 
                callback();
        }

        void OnCompleteInitialization()
        {
            m_InitializationIndex++;

            /* STUFF I DONT KNOW */
        }

        void InitSingleton()
        {
            if(s_ImageLibrary!=null)
            {
                Destroy(this.gameObject);
                return;
            }

            s_ImageLibrary = this;
        }
    }
}
