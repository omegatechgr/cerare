﻿namespace Cerare.Framework.Api
{
public class AccessResponse 
    {
        public int Result{ get; set;}
        public string SessionId{ get; set; }
        public string Message { get; set; }
    }
}
