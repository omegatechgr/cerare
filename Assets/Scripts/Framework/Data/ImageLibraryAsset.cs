﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Omega.Framework;
using Omega.Utils;
using System;

namespace Cerare.Framework.Data
{
    public class ImageLibraryAsset : IAsset
    {
        public Sprite Sprite { get; set; }
        public ImageType Type { get; set; }

        public int Id
        {
            get
            {
                return Id;
            }

            set
            {
                /*do nothing*/
            }
        }

        public static void CreateAsset(Texture texture, Action<ImageLibraryAsset> onCreateAsset)
        {
            ImageLibraryAsset imageLibraryAsset = new ImageLibraryAsset();
            imageLibraryAsset.Sprite = texture.ToSprite();
            onCreateAsset(imageLibraryAsset);
        }
    }
}
