﻿using System.Collections.Generic;
using Cerare.Framework.Data;

namespace Cerare.Framework.Data
{
	public class Product
	{
        public int FileSaveVersion{get;set;}
		public int Id{get; set;}
		public string Name{get; set;}
		public string Creator{get; set;}
        public Preset Preset{get;set;}
		public string DateCreated{get; set;}
		public string Comments{get; set;}
        public string Background {get; set;}
        public bool Storytelling{get; set;}
		public List<Asset> Assets{get; set;}

		public int StateAssetCount { get { return Assets == null ? 0 : Assets.Count; } }

		public Product()
		{
			//Default constructor
		}
	}
}