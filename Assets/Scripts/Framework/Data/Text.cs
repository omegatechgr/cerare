﻿namespace Cerare.Framework.Data
{
	[System.Serializable]
	public class Text: Asset 
	{
		public string Title {get; set;}
        public string Content {get; set;}
        public int? AudioId {get;set;}

		public Text ()
		{
			DataType = DataType.Text;
		}

        public Text(string title,string content,int audioId)
		{
			DataType = DataType.Text;
			Content = content;
            Title = title;
            AudioId = audioId;
		}
	}
}
