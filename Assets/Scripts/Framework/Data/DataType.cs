﻿namespace Cerare.Framework.Data
{
	[System.Serializable]
	public enum DataType
	{
		Text,
		Image,
		Video
	}
}
