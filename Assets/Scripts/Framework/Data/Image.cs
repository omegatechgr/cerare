﻿namespace Cerare.Framework.Data
{
	[System.Serializable]
	public class Image: Asset 
	{
        public string Title{ get; set; }
        public int? AudioId { get; set; }
        public ImageType ImageType { get; set; }

		public Image ()
		{
			DataType = DataType.Image;
		}

        public Image (string title ,ImageType imageType,int audioId)
		{
			DataType = DataType.Image;
            Title = title;
            ImageType = imageType;
            AudioId = audioId;
		}
	}
}
