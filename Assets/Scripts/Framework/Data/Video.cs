﻿namespace Cerare.Framework.Data
{
	[System.Serializable]
	public class Video: Asset 
	{
		public string Url {get; set;}
        public string Title { get; set; }

		public Video ()
		{
			DataType = DataType.Video;
		}

        public Video (string url,string title)
		{
			DataType = DataType.Video;
			Url = url;
            Title = title;
		}
	}
}
