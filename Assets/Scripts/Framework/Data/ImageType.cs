﻿namespace Cerare.Framework.Data
{
    public enum ImageType
    {
        Png,
        Jpg
    }
}