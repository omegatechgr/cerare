﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Cerare.Framework.Data
{
    public static class ScannedData
    {
        private static string m_creatorID;
        private static string m_markerID;

        public static string CreatorID 
        {
            get 
            {
                return m_creatorID;
            }
            set 
            {
                m_creatorID = value;
            }
        }

        public static string MarkerID 
        {
            get 
            {
                return m_markerID;
            }
            set 
            {
                m_markerID = value;
            }
        }
    }
}
