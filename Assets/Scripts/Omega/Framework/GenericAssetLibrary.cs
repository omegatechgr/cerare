﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Omega.Framework
{
    public interface IAsset
    {
        int Id { get; set; }
    }

    /// <summary>
    /// Generic asset library.
    /// T is the asset type and U is the downloaded type.
    /// </summary>
    public class GenericAssetLibrary<T,U> where T : IAsset
    {
        // Callbacks dictionary. We register the callbacks for the same asset
        Dictionary<int, List<Action<T>>> m_CallsDictionary;
        // Downloaded assets data
        Dictionary<int, T> m_AssetsDictionary;
        // The action to download the Asset
        // id:int, onSuccess:Action<int,U>, onError:<>
        Action<int, Action<int,U>, Action> m_DownloadMethod;
        // The action to create the IAsset from the downloaded Object
        Action<U, Action<T>> m_CreateIAssetMethod;
        // An action to be used when the requested asset doese not exists
        Action m_OnAssetNotExists;

        public GenericAssetLibrary (
            Action<int, Action<int, U>, Action> downloadMethod,
            Action<U, Action<T>> createIAssetMethod,
            Action onAssetNotExists = null
        )
        {
            m_CallsDictionary = new Dictionary<int, List<Action<T>>>();
            m_AssetsDictionary = new Dictionary<int, T>();
            m_DownloadMethod = downloadMethod;
            m_CreateIAssetMethod = createIAssetMethod;
            m_OnAssetNotExists = onAssetNotExists;
        }

        public List<T> GetAllStoredAssets ()
        {
            return m_AssetsDictionary.Values.ToList();
        }

        public List<T> GetStoredAssets(List<int> assetIdList)
        {
            if (assetIdList == null)
                return null;

            if (assetIdList.Count == 0)
                return null;

            List<T> assetList = new List<T>();

            for (int i = 0; i < assetIdList.Count; i++)
            {
                if (m_AssetsDictionary.ContainsKey(assetIdList[i]))
                    assetList.Add(m_AssetsDictionary[assetIdList[i]]);
            }

            if (assetList.Count == 0)
                return null;

            return assetList;
        }

        public void GetAsset (int assetId, Action<T> callback)
        {
            // if already exists
            if (m_AssetsDictionary.ContainsKey(assetId))
            {
                if(callback != null)
                    callback(m_AssetsDictionary[assetId]);
                
                return;
            }

            if (m_OnAssetNotExists != null)
                m_OnAssetNotExists();

            // if there is the same call in progress
            if(m_CallsDictionary.ContainsKey(assetId))
            {
                if (callback != null)
                    m_CallsDictionary[assetId].Add(callback);
                
                return;
            }

            // add callback
            m_CallsDictionary.Add(assetId, new List<Action<T>> { callback });

            // call backend API with OnGetAsset
            m_DownloadMethod(assetId, (id,asset)=>OnGetAsset(assetId,asset), ()=>OnGetAssetError(assetId));

        }

        void OnGetAsset (int assetId, U assetData)
        {
            m_CreateIAssetMethod(assetData, (asset) => OnCreateAsset(assetId, asset));
        }

        void OnGetAssetError (int assetId)
        {
            Debug.LogWarningFormat("OnGetAssetError id={0}", assetId);

            CallRegistredCallbacks(assetId);
        }

        void OnCreateAsset (int assetId, T asset)
        {
            if (asset != null)
            {
                asset.Id = assetId;
                AddOrUpdateAsset(asset);
            }
            else
            {
                Debug.Log("---- null assest");
            }

            CallRegistredCallbacks(assetId);
        }

        void CallRegistredCallbacks (int assetId)
        {
            // if there is no callbacks registred return 
            if (!m_CallsDictionary.ContainsKey(assetId))
                return;

            // call all the registred callbacks
            for (int i = 0; i < m_CallsDictionary[assetId].Count; i++)
            {
                Action<T> callback = m_CallsDictionary[assetId][i];

                if (callback != null)
                {
                    // if key is not in dictionary (when OnGetAssetError is called)
                    if (m_AssetsDictionary.ContainsKey(assetId))
                        callback(m_AssetsDictionary[assetId]);
                    else
                        callback(default(T));
                }
            }

            // remove registred callbacks
            m_CallsDictionary.Remove(assetId);
        }

        public void AddOrUpdateAsset (T asset)
        {
            if (m_AssetsDictionary.ContainsKey(asset.Id))
                m_AssetsDictionary[asset.Id] = asset;
            else
                m_AssetsDictionary.Add(asset.Id, asset);
        }

        public void AddOrUpdateAssets(List<T> assets)
        {
            foreach (var item in assets)
                AddOrUpdateAsset(item);
        }

        public void AddOrUpdateAsset(U asset, Action<U, Action<T>> createAssetDelegate)
        {
            createAssetDelegate(asset, AddOrUpdateAsset);
        }

        public void AddOrUpdateAssets(List<U> assets, Action<U, Action<T>> createAssetDelegate)
        {
            foreach (var item in assets)
                AddOrUpdateAsset(item, createAssetDelegate);
        }

        #region Add Multiple Assets With One Callback

        // Use this count and check if all assets have been downloaded
        int m_DownloadedAssetCounter;
        bool m_DownloadAssetsInProgress;

        public bool GetMultipleAssets(List<int> assetIdList, Action<List<T>> callback)
        {
            if (m_DownloadAssetsInProgress)
                return false;

            if (assetIdList == null)
            {
                callback(null);
                return true;
            }

            m_DownloadAssetsInProgress = true;
            m_DownloadedAssetCounter = 0;


            for (int i = 0; i < assetIdList.Count; i++)
            {
                GetAsset(assetIdList[i], (x) => CheckIfAllAssetsDownloaded(callback, assetIdList));
            }

            return true;
        }

        void CheckIfAllAssetsDownloaded(Action<List<T>> callback, List<int> assetIdList)
        {
            if (++m_DownloadedAssetCounter >= assetIdList.Count)
            {
                if (callback != null)
                    callback(GetStoredAssets(assetIdList));

                m_DownloadAssetsInProgress = false;
            }
        }

        #endregion
    }
}