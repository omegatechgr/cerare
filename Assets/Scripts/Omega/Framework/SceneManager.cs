﻿using System.Collections;
using UnityEngine;
using UnitySceneManagement = UnityEngine.SceneManagement;
using Prime31.MessageKit;
using BTH.Tools;

namespace Omega.Framework
{
    public class SceneManager : MonoBehaviour 
    {
        #pragma warning disable 649
        [SerializeField] FadeCanvasGroupUI fadeCanvasGroupUI;
        #pragma warning restore

        private bool isLoading = false;

    	// Use this for initialization
    	void Awake () 
        {
            MessageKit<string>.addObserver (MessageType.ChangeScene, StartLoadingNewScene);
    	}

        void OnDestroy ()
        {
            MessageKit<string>.removeObserver (MessageType.ChangeScene, StartLoadingNewScene);
        }
    	
        void StartLoadingNewScene (string sceneName)
        {
            if (isLoading)
            {
                MessageKit<bool>.post (MessageType.ChangeSceneEnded, false);
                return;
            }

            isLoading = true;

            StartCoroutine (LoadSceneAsynch(sceneName));
        }

        IEnumerator LoadSceneAsynch (string sceneName)
        {
            Debug.LogFormat ("Scene {0} loaded", sceneName);

            MessageKit<string>.post (MessageType.ChangeSceneStarted, sceneName);

            yield return new WaitForSeconds (fadeCanvasGroupUI.FadeIn ());

            yield return UnitySceneManagement.SceneManager.LoadSceneAsync (sceneName);

            MessageKit<bool>.post (MessageType.ChangeSceneEnded, true);

            yield return new WaitForSeconds (fadeCanvasGroupUI.FadeOut ());

            isLoading = false;
        }
    }
}