﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryManager
{
    public static string GetBatteryState()
    {
        string batteryState = "FULL";

        if (SystemInfo.batteryLevel > 0.30f && SystemInfo.batteryLevel < 0.75f)
            batteryState = "MEDIUM";
        else if(SystemInfo.batteryLevel < 0.30f)
            batteryState = "LOW";

        return batteryState;
    }
}
