﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InternetConnectionManager
{
    public static string GetInternetConnectionState()
    {
        string internetConnectionState = string.Empty;

        if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork)
            internetConnectionState = "DATA";
        else if (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
            internetConnectionState = "WIFI";
        else
            internetConnectionState = "N/A";
            

        return internetConnectionState;
    }
}