﻿using System;
using Omega.Settings;
using UnityEngine;

namespace Omega.Framework
{
    public class AppEngine : MonoBehaviour
    {
        // private singleton
        static AppEngine instance;

        // managed update
        public static Action ManagedUpdate;

        #pragma warning disable 649
        [SerializeField] AppSettings appSettings;
        #pragma warning restore

        InputManager inputManager;

        #region initialization
        // Use this for initialization
        void Awake ()
        {
            if(instance != null)
            {
                Debug.LogError("You can not have more than one instance of the AppEngine.");
                Destroy(this.gameObject);
                return;
            }

            instance = this;

            DontDestroyOnLoad(this.gameObject);

            init();
        }

        void init ()
        {
            // initialization code here...
            // ...

            inputManager = new InputManager();

            // from mobile versions
            Screen.sleepTimeout = SleepTimeout.NeverSleep;

            // localisation
            Omega.Localisation.LocalisationManager.LoadDicionary(
                appSettings.LocalisationDictionaryResourcesPath,
                appSettings.DefaultLocalisationLanguageCulture
            );
        }
        #endregion

        #region global access
        public static AppSettings Settings { get { return instanceExists ? instance.appSettings : null; } }

        // Add here more pubic static properties to access modules ...
        // ...

        #endregion



        void Update ()
        {
            inputManager.Update();

            if (ManagedUpdate != null)
                ManagedUpdate();
        }

        static bool instanceExists
        {
            get
            {
                if (instance != null)
                    return true;

                Debug.LogError("AppEngine instance is missing.");
                return false;
            }
        }
    }
}
