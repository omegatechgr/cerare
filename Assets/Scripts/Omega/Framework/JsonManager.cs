﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullSerializer;

namespace Omega.Utils
{
    public class JsonManager
    {
        public static T DeserializedJson<T>(T deserializedObject,string jsonContents)
        {
            fsData data = fsJsonParser.Parse (jsonContents);
            fsSerializer serializer = new fsSerializer ();
            serializer.TryDeserialize<T> (data,ref deserializedObject).AssertSuccessWithoutWarnings ();
            return deserializedObject;
        }

        public static string SerializedObject<T>(T objectToSerialize)
        {
            fsData data;
            fsSerializer serializer = new fsSerializer ();
            serializer.TrySerialize<T> (objectToSerialize, out data).AssertSuccessWithoutWarnings ();
            string jsonContent = data.ToString();
            return jsonContent;
        }
    }
}
