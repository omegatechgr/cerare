﻿using System.Collections;
using UnityEngine;
using Prime31.MessageKit;
using Omega.Framework;

namespace Omega.Framework
{
    public class ApplicationInit : MonoBehaviour 
    {
        #pragma warning disable 649
        [SerializeField]
        string firstSceneName;
        [SerializeField]
        float timeBeforeChangingScene = 2f;
        #pragma warning restore

        // Use this for initialization
        IEnumerator Start () 
        {
            yield return new WaitForSeconds (timeBeforeChangingScene);

            MessageKit<string>.post (MessageType.ChangeScene, firstSceneName);

    	}
    }
}
