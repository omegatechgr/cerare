﻿namespace Omega.Framework
{
    public static class MessageType 
    {
        // Scene Manager
        public static int ChangeScene = 10; //<SceneName:string>
        public static int ChangeSceneStarted = 11; //<SceneName:string>
        public static int ChangeSceneEnded = 12; //<Success:bool>

        // Login and User Permissions
        public static int SetPermissions = 20; //<UserRole>

        // AR Manager
        public static int ScannedNewMarker = 30;
        public static int MarkerLost = 31;
        public static int MarkerFound = 32;
    }
}
