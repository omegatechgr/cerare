﻿using System;
using UnityEngine;

namespace Omega.Framework
{
    public class InputManager
    {
        public Action<Vector2> OnLeftClick;
        public Action<Vector2> OnRightClick;
        public Action<Vector2, Vector2> OnPositionChange; // position, deltaPosition

        Vector2 prevMousePosition;

        public InputManager ()
        {
            prevMousePosition = Input.mousePosition;
        }

        public void Update()
        {
            if (OnPositionChange != null)
                OnPositionChange(Input.mousePosition, (Vector2)Input.mousePosition - prevMousePosition);

            prevMousePosition = Input.mousePosition;

            if(Input.GetMouseButtonUp(0))
            {
                if (OnLeftClick != null)
                    OnLeftClick(Input.mousePosition);
            }

            if (Input.GetMouseButtonUp(1))
            {
                if (OnLeftClick != null)
                    OnLeftClick(Input.mousePosition);
            }
        }
    }
}
