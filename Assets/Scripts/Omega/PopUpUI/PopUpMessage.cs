﻿using UnityEngine;
using System;
using UnityEngine.UI;

namespace Omega.PopUpUI
{
    public class PopUpMessage
    {
    	// call backs delegates
        public Action CloseCallBack {get; set;}
        public Action ShareCallBack {get; set;}
        public Action AcceptCallBack {get; set;}

    	public string Message 	{get; private set;}
        public string AcceptButtonText { get; private set; }
    	public Sprite BGSprite	{get; set;}
        public bool HideCloseButton { get; private set; }

        public Action<string> AcceptDialogueInputCallBack { get; private set;}
        public bool IsInputDialogue { get {return AcceptDialogueInputCallBack != null;}}
        public InputField.ContentType InputFiledContentType { get; private set; }

        public PopUpMessage(string message, Action<string> acceptDialogueInputCallBack, InputField.ContentType inputFiledContentType)
        {
            setMessage(message, null, null, null, acceptDialogueInputCallBack);
            InputFiledContentType = inputFiledContentType;
        }

        public PopUpMessage(string message, bool hideCloseButton = false)
    	{
            setMessage(message, null, null, null, null, hideCloseButton);
    	}
            
    	public PopUpMessage(string message, Action closeCallBack)
    	{
            setMessage(message, null, closeCallBack, null);
    	}
            
    	public PopUpMessage(string message, Action closeCallBack, Action acceptCallBack)
    	{
            setMessage(message, null, closeCallBack, acceptCallBack);
    	}

        public PopUpMessage(string message,
            string acceptButtonText,
            Action closeCallBack,
            Action acceptCallBack)
        {
            setMessage (message,
                acceptButtonText,
                closeCallBack,
                acceptCallBack);
        }

        void setMessage (string message, 
            string acceptButtonText,
            Action closeCallBack,
            Action acceptCallBack,
            Action<string> acceptDialogueInputCallBack = null,
            bool hideCloseButton = false
        )
        {
            Message         = message;
            AcceptButtonText = acceptButtonText;
            CloseCallBack   = closeCallBack;
            AcceptCallBack  = acceptCallBack;
            BGSprite        = null;
            HideCloseButton = hideCloseButton;
            AcceptDialogueInputCallBack = acceptDialogueInputCallBack;
        }


    	/// <summary>
    	/// Shows the message.
    	/// </summary>
        public PopUpMessage ShowMessage ()
    	{
    		PopUpMessageManager.AddMessage( this );
            return this;
    	}

        public void HideMessage ()
        {
            PopUpMessageManager.HideMessage (this);
        }

        public void UpdateMessage (string message)
        {
            Message = message;
            PopUpMessageManager.UpdateMessage (this);
        }

    }
}
