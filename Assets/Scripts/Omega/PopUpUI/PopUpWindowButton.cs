﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Omega.PopUpUI
{
    public class PopUpWindowButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        #pragma warning disable 649
        [SerializeField]
        Text labelText;
        #pragma warning restore

        PopUpButtonInfo info;
        bool pointerOnObject = false;

        public void SetButton (PopUpButtonInfo info)
        {
            pointerOnObject = false;
            this.info = info;
            labelText.text = info.Label;
            this.gameObject.SetActive (true);
        }

        public void Hide ()
        {
            this.gameObject.SetActive (false);
        }

        #region IPointerEnterHandler implementation

        public void OnPointerEnter (PointerEventData eventData)
        {
            pointerOnObject = true;
        }

        #endregion

        #region IPointerExitHandler implementation

        public void OnPointerExit (PointerEventData eventData)
        {
            pointerOnObject = false;
        }

        #endregion

        public bool CheckForAction ()
        {
            if (pointerOnObject && info.Action != null)
            {
                info.Action ();
                return true;
            }

            return false;
        }
    }
}
