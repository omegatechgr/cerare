﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

namespace Omega.PopUpUI
{

    public struct PopUpButtonInfo
    {
        public PopUpButtonInfo(string label, Action action)
        {
            Label = label;
            this.Action = action;
        }

        public string Label { get; private set;}
        public Action Action {get; private set;}

        public void AddAction (Action action)
        {
            Action += action;
        }
    }

    public class PupUpWindowManager : MonoBehaviour
    {
        private static PupUpWindowManager instace;

        #pragma warning disable 649
        [SerializeField]
        GameObject popUpObject;
        [SerializeField]
        Transform buttonsContainer;
        [SerializeField]
        PopUpWindowButton buttonPrefab;
        #pragma warning restore

        List<PopUpWindowButton> buttonsList = new List<PopUpWindowButton>();

    	// Use this for initialization
    	void Awake () 
        {
            if (instace != null)
            {
                Destroy (this.gameObject);
                return;
            }

            instace = this;

            DontDestroyOnLoad (this.gameObject);

            Hide ();
    	}
    	
        void Update ()
        {
            if (Input.GetMouseButtonUp(1))
            {
                CheckForAction ();
                Hide ();
            }
        }


        /// <summary>
        /// Shows the window.
        /// </summary>
        /// <returns><c>true</c>, if window was shown, <c>false</c> otherwise.</returns>
        /// <param name="windowInfo">Window info.</param>
        /// <param name="position">Position.</param>
        public static bool ShowWindow (PopUpButtonInfo[] windowInfo, Vector2 position)
        {
            if (instace == null)
                return false;

            if (windowInfo == null)
                return false;

            instace.CreateButtons (windowInfo);
            instace.Locate (position);
            instace.Show ();

            return true;
        }

        void Show ()
        {
            popUpObject.SetActive (true);
        }

        void Hide ()
        {
            popUpObject.SetActive (false);
        }

        void CheckForAction ()
        {
            foreach (PopUpWindowButton button in buttonsList)
            {
                if (button.CheckForAction ())
                    return;
            }
        }

        void CreateButtons (PopUpButtonInfo[] windowInfo)
        {
            PopUpWindowButton[] buttons = buttonsContainer.GetComponentsInChildren<PopUpWindowButton> (true);
            buttonsList.Clear ();

            // pooling
            for (int i = 0; i < windowInfo.Length; i++)
            {
                // add hide action
                //windowInfo[i].AddAction (Hide);

                if (i < buttons.Length)
                {
                    buttons[i].SetButton (windowInfo[i]);
                    buttonsList.Add (buttons[i]);
                }
                else
                {
                    PopUpWindowButton button = Instantiate<PopUpWindowButton> (buttonPrefab, buttonsContainer);
                    button.SetButton (windowInfo[i]);
                    buttonsList.Add (button);
                }
            }

            // hide rest if any
            for (int i = windowInfo.Length; i <buttons.Length; i++)
            {
                buttons[i].Hide ();
            }
        }


        void Locate (Vector2 position)
        {
            //Debug.LogFormat ("Locate popup window {0}", position.ToString ());
            popUpObject.transform.position = position;
        }
    }
}
