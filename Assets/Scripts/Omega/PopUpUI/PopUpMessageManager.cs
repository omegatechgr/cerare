﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

namespace Omega.PopUpUI
{
    public class PopUpMessageManager : MonoBehaviour 
    {
    	private static Queue<PopUpMessage> messagesQueue = new Queue<PopUpMessage>(); 
        private static PopUpMessage currentMessage = null;
        private static PopUpMessageManager instance;

#pragma warning disable 649
        [SerializeField]
        private GameObject popUpObject;
        [SerializeField]
        private Text messageText;
        [SerializeField]
        private Text acceptButtonText;
        [SerializeField]
        private Image bgImage;
        [SerializeField]
        private Button	acceptButton;
        [SerializeField]
        private Button closeButton;
        [SerializeField] InputField inputField;
#pragma warning restore

        private bool showNextMessage = true;
        private Action closeCallBack;
        private Action acceptCallBack;
        private Action<string> acceptDialogueInputCallBack;


    	/// <summary>
    	/// Adds the message.
    	/// </summary>
    	/// <param name="message">Message.</param>
    	public static void AddMessage (PopUpMessage message)
    	{
    		messagesQueue.Enqueue( message );
    	}

        public static void HideMessage (PopUpMessage message)
        {
            if (currentMessage != message)
                return;

            instance.CloseMessage ();
        }

        public static void UpdateMessage (PopUpMessage message)
        {
            if (currentMessage != message)
                return;

            instance.UpdateMessage (message.Message);
        }

    	// Use this for initialization
    	void Awake () 
    	{
            if (instance == null)
                instance = this;

            acceptButton.onClick.AddListener( AcceptButtonAction );
            closeButton.onClick.AddListener( CloseButtonAtion );

    		CloseMessage();

            DontDestroyOnLoad (this.gameObject);
    	}

        void OnDestroy ()
        {
            acceptButton.onClick.RemoveAllListeners();
            closeButton.onClick.RemoveAllListeners();
        }

    	// Update is called once per frame
    	void Update () 
    	{
    		if( messagesQueue.Count > 0 && showNextMessage)
    		{
    			showNextMessage = false;
    			ShowMessage(messagesQueue.Dequeue());
    		}
    	} 

    	/// <summary>
    	/// Shows the message.
    	/// </summary>
    	/// <param name="message">Message.</param>
    	void ShowMessage (PopUpMessage message)
    	{
            currentMessage = message;

            inputField.text = string.Empty;
            inputField.contentType = message.InputFiledContentType;
            inputField.gameObject.SetActive (message.IsInputDialogue);
            closeButton.gameObject.SetActive (!message.HideCloseButton);
    		messageText.text = message.Message;

            if (acceptButtonText != null)
            {
                if (message.AcceptButtonText == null)
                {
                    // TODO: show accept text
                }
                else
                {
                    Debug.LogFormat ("{0}", message.AcceptButtonText);
                    acceptButtonText.text = message.AcceptButtonText;
                }
            }

    		closeCallBack = message.CloseCallBack;
    		acceptCallBack = message.AcceptCallBack;
            acceptDialogueInputCallBack = message.AcceptDialogueInputCallBack;
            acceptButton.gameObject.SetActive(acceptCallBack != null || acceptDialogueInputCallBack != null);

            if (bgImage != null)
            {
                bgImage.sprite = message.BGSprite;

                if (message.BGSprite == null)
                {
                    bgImage.enabled = false;
                }
                else
                {
                    bgImage.enabled = true;
                }
            }

    		// show message
            popUpObject.SetActive(true);
    	}

        void UpdateMessage (string message)
        {
            messageText.text = message;
        }

    	void CloseMessage ()
    	{
    		popUpObject.SetActive(false);
            currentMessage = null;
    		showNextMessage = true;
    	}

    	/// <summary>
    	/// Closes the button ation.
    	/// </summary>
    	void CloseButtonAtion ()
    	{
    		if( closeCallBack != null )
    		{
    			closeCallBack();
    			closeCallBack = null;
    		}

    		CloseMessage();
    	}

    	/// <summary>
    	/// Accepts the button action.
    	/// </summary>
    	void AcceptButtonAction ()
    	{
            if (inputField.isActiveAndEnabled)
            {
                if (acceptDialogueInputCallBack != null)
                {
                    acceptDialogueInputCallBack (inputField.text);
                    acceptDialogueInputCallBack = null;
                }
            }
    		else if( acceptCallBack != null )
    		{
    			acceptCallBack();
    			acceptCallBack = null;
    		}

    		CloseMessage();
    	}

    	

    }
}
