﻿using UnityEngine;

namespace Melos.UI
{
    [AddComponentMenu("Melos/UI/Image Relocate OnStart")]
    public class UIImageRelocateOnStart : MonoBehaviour 
    {
        [SerializeField]
        bool deactivateOnStart = false;

    	// Use this for initialization
    	void Awake () 
        {
            RectTransform rectTransform = GetComponent<RectTransform>();
            rectTransform.anchoredPosition = Vector2.zero;

            if (deactivateOnStart)
                this.gameObject.SetActive (false);
    	}
    }
}
