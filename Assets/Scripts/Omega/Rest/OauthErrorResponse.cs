﻿using FullSerializer;

namespace Omega.Rest
{
    public class OauthErrorResponse
    {
        public string Error { get; set; }
        public string Description { get; set; }
    }
}