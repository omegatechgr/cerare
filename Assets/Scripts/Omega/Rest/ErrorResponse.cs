﻿namespace Omega.Rest
{
    public class ApiErrorResponse
    {
        public int Code;
        public string Message;
        public string Tag;
    }
}