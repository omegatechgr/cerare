﻿using System.Text;
using UnityEngine;
using System.Collections.Generic;
using Cerare.Framework.Data;

namespace Omega.Utils
{
    public static class Extensions
    {
        public static byte[] GetBytes(this string str)
        {
            return Encoding.UTF8.GetBytes(str);
        }

        public static string GetString(this byte[] bytes)
        {
            return Encoding.UTF8.GetString(bytes);
        }

        public static Sprite ToSprite(this Texture texture)
        {
            return ToSprite((Texture2D)texture);
        }

        public static Sprite ToSprite(this Texture2D texture)
        {
            Sprite sprite = Sprite.Create(texture,
                                          new Rect(0, 0, texture.width, texture.height),
                                          new Vector2(0.5f, 0.5f));
            sprite.name = texture.name;

            return sprite;
        }

        public static Dictionary<int,Asset> ToIdAssetDictionary(this List<Asset> assets)
        {
            Dictionary<int, Asset> dictionary = new Dictionary<int, Asset>();

            for (int i = 0; i < assets.Count; i++)
            {
                if(!dictionary.ContainsKey(assets[i].Id))
                    dictionary.Add(assets[i].Id, assets[i]);
            }

            return dictionary;
        }

        public static List<int> ToIdsList (this List<Asset> assets)
        {
            List<int> assetIds = new List<int>();

            for (int i = 0; i < assets.Count; i++)
            {
                if(!assetIds.Contains(assets[i].Id))
                    assetIds.Add(assets[i].Id);
            }

            return assetIds.Count == 0 ? null : assetIds;
        }
    }
}