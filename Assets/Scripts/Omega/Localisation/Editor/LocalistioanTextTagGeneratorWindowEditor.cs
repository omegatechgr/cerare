﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using LitJson;
using Omega.Localisation;
using System.Text;

namespace Omega.Localisation.Editor
{
    public class LocalistioanTextTagGeneratorWindowEditor : EditorWindow
    {
        private const string textTagPath = "Assets/Text Tags/";
        private static EditorWindow window;
        private TextAsset dictionaryTextAsset;
        private StringBuilder resultStringBuilder = new StringBuilder();


        [MenuItem("Omega/TextTag Generator", false, 1)]
        static void ShowWindow ()
        {
            window = EditorWindow.GetWindow(typeof(LocalistioanTextTagGeneratorWindowEditor));
            window.titleContent = new GUIContent ("TextTag Generator");
            window.Show();
        }
    	
        void OnGUI ()
        {
            EditorGUILayout.BeginHorizontal();
            dictionaryTextAsset = (TextAsset)EditorGUILayout.ObjectField(dictionaryTextAsset, typeof(TextAsset), true);
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Generate"))
            {
                if (dictionaryTextAsset == null)
                {
                    ShowNotification (new GUIContent ("No dictionaey selected."));
                }
                else
                {
                    GenerateTextTags ();
                }
            }

            EditorGUILayout.TextArea (resultStringBuilder.ToString ());
        }

        void GenerateTextTags ()
        {
            int generateTextTagCount = 0;

            resultStringBuilder.Length = 0;
            resultStringBuilder.AppendLine ("Start generating text tags...");

            // get all text tags from assets
            List<string> textTagList = new List<string>();
            string[] guids = AssetDatabase.FindAssets("t:LocalisationTextTag");
            foreach (string guid in guids)
            {
                textTagList.Add (AssetDatabase.LoadAssetAtPath<LocalisationTextTag> (AssetDatabase.GUIDToAssetPath (guid)).name.ToUpper() );
            }

            // get data from json dictionary
            Dictionary<string,string> dictionary = new Dictionary<string, string>();
            dictionary.Clear();
            dictionary = JsonMapper.ToObject<Dictionary<string,string>>(dictionaryTextAsset.text);

            foreach (string key in dictionary.Keys)
            {
                if (textTagList.Contains (key.ToUpper ()))
                {
                    resultStringBuilder.AppendFormat ("{0} already in assets.\n", key.ToUpper ());
                }
                else
                {
                    LocalisationTextTag textTag = ScriptableObject.CreateInstance<LocalisationTextTag> ();
                    textTag.name = key.ToUpper ();
                    AssetDatabase.CreateAsset(textTag, string.Format("{0}{1}.asset",textTagPath, key.ToUpper()));
                    resultStringBuilder.AppendFormat ("{0} created.\n", key.ToUpper ());
                    generateTextTagCount++;
                }       
            }

            resultStringBuilder.AppendFormat ("{0} TextTags generated.", generateTextTagCount);
        }
    }
}
