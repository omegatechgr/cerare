﻿using UnityEngine;
using UnityEngine.UI;
namespace Omega.Localisation
{
    public class LocalisationSimpleUI : MonoBehaviour 
    {
        #pragma warning disable 649
        [SerializeField]
    	Text textUI;
        [SerializeField]
        LocalisationTextTag textTag;
        #pragma warning restore

        void Start ()
    	{
            if (textUI == null)
            {
                textUI = this.GetComponent<Text> ();
            }

            Translate ();

            LocalisationManager.onLanguageChange += Translate;
    	}

    	void OnDestroy ()
    	{
            LocalisationManager.onLanguageChange -= Translate;
    	}

    	//[ContextMenuItem("Translate","Translate")]
    	void Translate ()
    	{
            if (textUI == null)
            {
                Debug.LogWarningFormat ("Text componennt is missing : {0}", this.name);
                return;
            }

            textUI.text = textTag.T ();
    	}

    }
}
