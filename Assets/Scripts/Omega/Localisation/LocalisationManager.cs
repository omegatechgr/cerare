﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System;

namespace Omega.Localisation
{
    /// <summary>
    /// Language interface.
    /// </summary>
    public static class LocalisationManager
    {
    	// public static variables
        public static Action onLanguageChange;

        public static string CurrectLanguageCulture { get; private set; }

    	// dictionary
    	private static Dictionary<string,string> dictionary = new Dictionary<string, string>();


    	/// <summary>
    	/// Loads the dicionary.
    	/// </summary>
    	/// <returns><c>true</c>, if dicionary was loaded, <c>false</c> otherwise.</returns>
    	/// <param name="languageCulture">Language culture.</param>
    	public static bool LoadDicionary (string path, string languageCulture)
    	{
            Debug.LogFormat ("<color=green>LoadDicionary {0}</color>", languageCulture);

    		string _path = path + languageCulture;

    		TextAsset jsonData = Resources.Load<TextAsset>(_path);

    		if( jsonData == null )
    		{
    			Debug.LogWarningFormat("Can't load {0} from resources.", _path);
    			return false;
    		}

            CurrectLanguageCulture = languageCulture;

    		dictionary.Clear();

    		dictionary = JsonMapper.ToObject<Dictionary<string,string>>(jsonData.text);

            if( onLanguageChange != null)
    		{
                onLanguageChange.Invoke();
    		}

    		return true;
    	}
            
        /// <summary>
        /// Extension : T the specified key.
        /// </summary>
        /// <param name="key">Key.</param>
        public static string T (this string key)
        {
            string keyUpper = key.ToUpper ();

            if( !dictionary.ContainsKey(keyUpper) )
            {
                return keyUpper + "_NO_KEY_";
            }

            return dictionary[keyUpper];
        }
    }
}