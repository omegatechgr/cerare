﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Omega.Localisation
{
    [CreateAssetMenu(fileName= "LTT_",menuName = "Text Tag")]
    public class LocalisationTextTag : ScriptableObject
    {
        public string Id
        {
            get
            {
                return this.name.ToUpper ();
            }
        }

        public string T()
        {
            return Id.T ();
        }

       
    }
}
