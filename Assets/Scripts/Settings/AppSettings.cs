﻿using UnityEngine;

namespace Omega.Settings
{
    [CreateAssetMenu(menuName = "TemplateProject/Settings/Application")]
    public class AppSettings : ScriptableObject
    {
        [Header("General")]
        public string AppVersion;
        public string LocalisationDictionaryResourcesPath;
        public string DefaultLocalisationLanguageCulture;
        public int SaveFileVersion;

        [Header("Rest")]
        public string RestHost;
        public string SecretAppID;
        public string ApiKey;

        [Header ("Testing")]
        public string ProductID;
        public string CeramistID;
    }
}