﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Omega.Settings;
using Omega.Framework;
using Cerare.Framework.Managers;
using Cerare.Framework.Data;

public class RestManager_Testing : MonoBehaviour {


    [SerializeField] private string m_sessionId;
    [SerializeField] private Product m_product;
    [SerializeField] private RawImage m_imageHolder;
    [SerializeField] private AudioSource m_audioSource;
    [SerializeField] private List<Asset> productAssets = new List<Asset>();
    private void Awake()
    {
        #if !CERARE_TESTING
        Destroy(this.gameObject);
        #endif
    }

    private void Start()
    {
        RestManager.GetAccess(AppEngine.Settings.SecretAppID,
            accessResponse =>
            {
                m_sessionId = accessResponse.SessionId;

                RestManager.GetProduct(AppEngine.Settings.CeramistID, AppEngine.Settings.ProductID,
                    product =>
                    {
                        m_product = product;
                        Debug.LogFormat("Got Product {0} from {1} with id {2} ",product.Name,product.Creator,product.Id);
                        for (int i = 0; i < m_product.StateAssetCount; i++)
                        {
                            if(m_product.Assets[i].GetType() == typeof(Cerare.Framework.Data.Image))
                            {
                                Debug.LogFormat("Asset is type of {0}",m_product.Assets[i].GetType());
                                Cerare.Framework.Data.Image productImage = (Cerare.Framework.Data.Image)m_product.Assets[i];
                                productAssets.Add(productImage);
                                RestManager.GetImage(productImage.Id,
                                    texture=>
                                    {
                                        m_imageHolder.texture = texture;
                                    },
                                    error=>
                                    {
                                        Debug.LogFormat("Rest Error \n Code: {0} \n Error Description: {1}",error.Code,error.Description);
                                    });
                                if (productImage.AudioId.HasValue)
                                {
                                    RestManager.GetAudio(productImage.AudioId.Value, audioClip =>
                                         {
                                             m_audioSource.PlayOneShot(audioClip, 1f);
                                         }, error =>
                                         {
                                            Debug.LogFormat("Rest Error \n Code: {0} \n Error Description: {1}", error.Code, error.Description);
                                         });
                                }
                            }
                            else if(m_product.Assets[i].GetType() == typeof(Cerare.Framework.Data.Text))
                            {
                                Cerare.Framework.Data.Text productText = (Cerare.Framework.Data.Text)m_product.Assets[i];
                                productAssets.Add(productText);
                                if (productText.AudioId.HasValue)
                                {
                                    RestManager.GetAudio(productText.AudioId.Value, audioClip =>
                                        {
                                            m_audioSource.PlayOneShot(audioClip, 1f);
                                        }, error =>
                                        {
                                            Debug.LogFormat("Rest Error \n Code: {0} \n Error Description: {1}", error.Code, error.Description);
                                        });
                                }
                            }
                        }
                    },
                    error =>
                    {
                        Debug.LogFormat("Rest Error \n Code: {0} \n Error Description: {1}",error.Code,error.Description);
                    });
            }
            , error =>
            {
                Debug.LogFormat("Rest Error \n Code: {0} \n Error Description: {1}",error.Code,error.Description);
            });
    }
}
