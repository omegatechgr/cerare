﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Omega.Utils;
using Cerare.Framework.Data;
public class CerareTest : MonoBehaviour {

    public TextAsset testJson;
    public Product testState;
    private void Start()
    {
        testState = new Product ();
        testState = JsonManager.DeserializedJson<Product>(testState, testJson.text);
        string result = JsonManager.SerializedObject<Product> (testState);
    }
}
