﻿using UnityEngine;
using System.Collections;
using System;

namespace BTH.Tools
{
    [AddComponentMenu("BTH/Fade Canvas Group UI")]
	public class FadeCanvasGroupUI : MonoBehaviour 
	{
		// public variables
		public CanvasGroup	canvasGroup;
		public bool			visible						= false;
		public float		minAlphaValue				= 0f;
		public float		maxAlphaValue 				= 1f;
		public float		fadeDuration				= 0.5f;
		public bool			blocksRaycastsWhenVisible 	= true;
		public GameObject[]	gameObjectsToDisable;

		// private variables
        private LerpAnimation lerpAnim;
		private IEnumerator coroutine;


		void SetAtivateObjects (bool value)
		{
            if (gameObjectsToDisable != null)
            {
                foreach (GameObject _obj in gameObjectsToDisable)
                {
                    if (_obj != null)
                    {
                        _obj.SetActive (value);
                    }
                }
            }
		}

		// Use this for initialization
		void Start () 
		{
			lerpAnim = this.gameObject.AddComponent<LerpAnimation>();

			if(canvasGroup == null)
			{
				canvasGroup = this.GetComponent<CanvasGroup>();
			}

			if(canvasGroup == null)
			{
                Debug.LogWarning("Canvas Group missing :: " + this.gameObject.name);
				return;
			}

			//HideImmediately();

			if(visible)
			{
				ShowImmediately();
			}
			else
			{
				HideImmediately();
			}
		}

		void Update ()
		{
			if(blocksRaycastsWhenVisible)
			{
				if(canvasGroup.alpha > maxAlphaValue - 0.1f)
				{
					canvasGroup.blocksRaycasts	= true;
				}

				if(canvasGroup.alpha < minAlphaValue + 0.1f)
				{
					canvasGroup.blocksRaycasts	= false;
				}

			}
		}

		public void HideImmediately ()
		{
			
			/*
			if(!behaveIsDone)
				return;
			*/

			// stop coroutine
			if( coroutine != null )
			{
				StopCoroutine( coroutine );
			}

			canvasGroup.alpha 			= minAlphaValue;
			visible						= false;

			if( canvasGroup.alpha < 0.001f )
			{
				SetAtivateObjects(false);
			}

			/*
			if(blocksRaycastsWhenVisible)
			{
				canvasGroup.blocksRaycasts	= false;
			}
			*/
		}
		public void ShowImmediately ()
		{

			
			/*
			if(!behaveIsDone)
				return;
			*/

			// stop coroutine
			if( coroutine != null )
			{
				StopCoroutine( coroutine );
			}

			SetAtivateObjects(true);
			canvasGroup.alpha 			= maxAlphaValue;
			visible						= true;

			/*
			if(blocksRaycastsWhenVisible)
			{
				canvasGroup.blocksRaycasts	= true;
			}
			*/
		}

		// UI Triggers
		public void FadeInUITrigger ()
		{
			FadeIn ();
		}
		public void FadeOutUITrigger ()
		{
			FadeOut ();
		}

		// Fade In
        public float FadeIn ()
		{

			// stop coroutine
			if( coroutine != null )
			{
				StopCoroutine( coroutine );
			}

			// start coroutine
			coroutine = FadeInIEnumerator();
			StartCoroutine( coroutine );

			return fadeDuration;
		}
		private IEnumerator FadeInIEnumerator ()
		{
			SetAtivateObjects(true);

			lerpAnim.SmoothStep( canvasGroup.alpha, maxAlphaValue, fadeDuration);

			while(lerpAnim.isAnimate)
			{
				canvasGroup.alpha = lerpAnim.currentValue;
				yield return null;
			}

			canvasGroup.alpha = maxAlphaValue;

			/*
			if(blocksRaycastsWhenVisible)
			{
				canvasGroup.blocksRaycasts	= true;
			}
			*/

			visible = true;

		}

		// Fade In
        public float FadeOut (Action callback = null)
		{
            return FadeOut(fadeDuration, callback);
		}
        public float FadeOut (float duration, Action callback = null)
		{
			// stop coroutine
			if( coroutine != null )
			{
				StopCoroutine( coroutine );
			}
			
            StartCoroutine( FadeOutIEnumerator(duration, callback) );
			
			return duration;
		}
        private IEnumerator FadeOutIEnumerator (float duration, Action callback)
		{
			lerpAnim.SmoothStep( canvasGroup.alpha, minAlphaValue, duration);
			
			while(lerpAnim.isAnimate)
			{
				canvasGroup.alpha = lerpAnim.currentValue;
				yield return null;
			}

			canvasGroup.alpha = minAlphaValue;


			visible = false;

			if( canvasGroup.alpha < 0.001f )
			{
				SetAtivateObjects(false);
			}

            if (callback != null)
            {
                callback ();
            }
			
		}
	}

}
