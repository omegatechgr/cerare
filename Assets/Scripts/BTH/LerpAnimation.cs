﻿using UnityEngine;

namespace BTH.Tools
{

	public class LerpAnimation : MonoBehaviour 
	{
		// get/set
		public bool 	isAnimate 		{get; private set;}
		public float	currentValue 	{get; set;}

		// private enums
		private enum LERP_TYPE 
		{ 
			LERP		=	0, 
			SMOOTHSTEP	=	1 
		};

		// private variable
		private LERP_TYPE 	lerpType;
		private float		beginValue;
		private float		targetValue;
		private float		timer;
		private	float		duration;


		// Initr
		void Start ()
		{
			isAnimate = false;
		}
		
		// LERP
		public void Lerp (float to, float duration)
		{
			isAnimate			= false;
			beginValue		= currentValue;
			targetValue		= to;
			this.duration	= duration;
			timer			= 0f;
			lerpType		= LERP_TYPE.LERP;
			isAnimate		= true;
		}
		public void SmoothStep (float to, float duration)
		{
			isAnimate			= false;
			beginValue		= currentValue;
			targetValue		= to;
			this.duration	= duration;
			timer			= 0f;
			lerpType		= LERP_TYPE.SMOOTHSTEP;
			isAnimate		= true;
		}
		public void Lerp (float from, float to, float duration)
		{
			isAnimate			= false;
			currentValue	= from;
			beginValue		= currentValue;
			targetValue		= to;
			this.duration	= duration;
			timer			= 0f;
			lerpType		= LERP_TYPE.LERP;
			isAnimate		= true;
		}
		public void SmoothStep (float from, float to, float duration)
		{
			isAnimate		= false;
			currentValue	= from;
			beginValue		= currentValue;
			targetValue		= to;
			this.duration	= duration;
			timer			= 0f;
			lerpType		= LERP_TYPE.SMOOTHSTEP;
			isAnimate		= true;
		}
		
		// Update is called once per frame
		void Update () 
		{
			if( isAnimate )
			{
				timer += Time.deltaTime;
				switch( lerpType )
				{
				case LERP_TYPE.LERP:
					currentValue = Mathf.Lerp(beginValue, targetValue, timer / duration);
					break;
				case LERP_TYPE.SMOOTHSTEP:
					currentValue = Mathf.SmoothStep(beginValue, targetValue, timer / duration);
					break;
				}

				if(timer >= duration)
				{
					isAnimate = false;
				}
			}
		}
	}
}
