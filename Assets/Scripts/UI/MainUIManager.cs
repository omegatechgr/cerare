﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Prime31.MessageKit;
using Omega.Framework;
namespace Cerare.UI
{
    public class MainUIManager : MonoBehaviour 
    {
        #pragma warning disable 649
        [SerializeField] private Button m_startButton;
        [SerializeField] private Button m_infoButton;
        [SerializeField] private Button m_settingsButton;
        [SerializeField] private Image m_wifiImage;
        [SerializeField] private Image m_dataImage;
        [SerializeField] private Image m_lowBatteryImage;
        [SerializeField] private Image m_mediumBatteryImage;
        [SerializeField] private Image m_highBatteryImage;
        [SerializeField] private string qrScannerSceneName;
        #pragma warning restore
    
        private void Awake()
        {
            m_startButton.onClick.AddListener (startButtonAction);
            setupBattery ();
            setupInternetConnection ();
        }

        void startButtonAction()
        {
            MessageKit<string>.post (MessageType.ChangeScene, qrScannerSceneName);
        }

        void setupBattery()
        {
            if (BatteryManager.GetBatteryState () == "FULL")
                m_highBatteryImage.gameObject.SetActive (true);
            else if (BatteryManager.GetBatteryState () == "MEDIUM")
                m_mediumBatteryImage.gameObject.SetActive (true);
            else
                m_lowBatteryImage.gameObject.SetActive (true);
        }

        void setupInternetConnection()
        {
            if (InternetConnectionManager.GetInternetConnectionState() == "DATA")
            {
                m_dataImage.gameObject.SetActive (true);
            }
            else if (InternetConnectionManager.GetInternetConnectionState() == "WIFI")
            {
                m_wifiImage.gameObject.SetActive(true);
            }
        }
    }
}
